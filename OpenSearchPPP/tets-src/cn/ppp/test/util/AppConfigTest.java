package cn.ppp.test.util;

import org.junit.Assert;
import org.junit.Test;

import com.ppp.searchHelp.util.AppConfig;

public class AppConfigTest {

	@Test
	public void test(){
		
		Assert.assertEquals(AppConfig.getStr("noKey", "hehe"), "hehe");		
		Assert.assertEquals(AppConfig.getLong("period"), new Long(120));		
	}
}
