package cn.ppp.test.util;

import org.junit.Assert;
import org.junit.Test;

import com.ppp.searchHelp.util.StringUtil;

public class StringUtilTest {

	
	@Test
	public void testIsBlank(){
		
		Assert.assertTrue(StringUtil.isBlank(" "));
		Assert.assertTrue(StringUtil.isBlank(""));
		Assert.assertTrue(StringUtil.isBlank(null));
		Assert.assertFalse(StringUtil.isBlank("ss "));
		
	}
	
	@Test
	public void testStringToMap(){
		String json = "{'peron':{'name':'song','age':17},'type':'json'}";
		
		Assert.assertNotNull(StringUtil.stringToMap(json));
		
	}
	
}
