package cn.ppp.test.core;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import org.apache.http.client.ClientProtocolException;
import org.junit.Test;

import com.ppp.searchHelp.core.CloudsearchDocHelp;
import com.ppp.searchHelp.util.StringUtil;

/**
 * 测试文档帮助类上传文档测试
 * 
 * 
 * 
 * @author song
 *
 */
public class AddDocHelpTest {

	
	@Test
	public void testDetail() {

		CloudsearchDocHelp help = new CloudsearchDocHelp();
		help.getDetail("301");
		help.getDetail("303");
		help.getDetail("304");
	}
	 
	
	/**
	 * 从文本中读取文字以5个字为单位分割，随机组成文档作为数据源上传。上传的后的数据写入到新的文本中方便测试
	 */
	//@Test
	public void testAdd() throws ClientProtocolException, IOException {

		CloudsearchDocHelp help = new CloudsearchDocHelp();

		Map<String, Object> fields = null;
		read();
		init();
		Random rand = new Random();
		String str="";
		String body = "";
		for (int id = 1; id < 300; id++) {
			fields = new HashMap<>();
			fields.put("id", id);
			str =  randName(1);
			fields.put("title", str);
			body = randName(rand.nextInt(10) + 5);
			fields.put("body", body);
			help.add(fields);
			write("id: " + str +",body: " +body + "\n");
		}
		help.push();
		close();
	}

	private List<String> strs = new ArrayList<>();

	private void read() throws IOException {

		File file = new File("d:\\text.txt");
		InputStreamReader read = new InputStreamReader(new FileInputStream(file),"GBK");
		BufferedReader bufferedReader = new BufferedReader(read);
		String lineTxt = null;
		while ((lineTxt = bufferedReader.readLine()) != null) {
			if (!StringUtil.isBlank(lineTxt)) {
				sub5(lineTxt);
			}
		}
		read.close();
	}
	
	//@Test
	public void testWriteRead() throws IOException{
		read();
		init();
		System.out.println(strs.size());
		System.out.println(randName(1));
		System.out.println(randName(3));
		write("你好1");
		write("你好2 \n");
		write("你好3");
		close();
	}

	private OutputStream out = null;
	private void init() throws IOException{
		File file = new File("d:\\write.txt");
		out = new FileOutputStream(file);
		
	}
	private void close(){
		try {
			out.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	private void write(String s) throws IOException {
		byte b[] = s.getBytes();
		out.write(b);
	}

	private void sub5(String str) {
		if (str.length() > 5) {
			strs.add(str.substring(0, 5));
			sub5(str.substring(5));
		} else {
			strs.add(str);
		}

	}

	private String randName(int type) {
		int max = strs.size();
		Random rand = new Random();
		if (type < 2) {
			return strs.get(rand.nextInt(max));
		} else {
			type--;
			return strs.get(rand.nextInt(max)) + randName(type);
		}

	}
}
