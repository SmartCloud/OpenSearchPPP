package cn.ppp.test.core;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import com.ppp.searchHelp.core.PPPCore;

public class TestPPPCore {

	private static PPPCore ppp = PPPCore.getMe();
	
	@Test
	public void getClient(){
		
		assertNotNull(ppp.getClient());
	}
	
	@Test
	public void getIndex() {
		assertNotNull(ppp.getSearchIndex());
	}

	@Test
	public void checkAppStatus(){
		assertTrue(ppp.checkAppStatus());
	}
}
