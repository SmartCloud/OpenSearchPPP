package com.ppp.searchHelp.DB;

import java.util.TimerTask;

public class DBTask extends TimerTask {

	@Override
	public void run() {
		new DBOperating().run();
	}

}
