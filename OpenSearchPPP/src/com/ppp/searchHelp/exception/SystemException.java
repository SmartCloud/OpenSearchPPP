package com.ppp.searchHelp.exception;

/**
 * 自定义异常
 * 
 * 
 * 用于参数错误等
 * @author song
 *
 */
public class SystemException extends RuntimeException{

	private static final long serialVersionUID = 1L;
	
	public SystemException(){
		super();
	}
	public SystemException(String msg){
		super(msg);
	}
	
}
