package com.ppp.searchHelp.core;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;

import org.json.JSONObject;

import com.ppp.searchHelp.util.StringUtil;

/**
 * ajax处理
 * 
 * @author song
 *
 */
public class Action {

	HttpServletRequest request;
	String parameter;

	public Action(HttpServletRequest req) {
		request = req;
		parameter = request.getParameter("query");
	}

	/**
	 * 执行搜索
	 * 
	 * 执行过程发生异常返回  ppp自定义错误 其status为"ERROR"
	 * 
	 */
	public String exec() {
		JSONObject resultObj = new JSONObject().put("status", "ERROR").put("remark", "ppp自定义错误");

		if (StringUtil.isBlank(parameter)) {
			resultObj.put("error", "缺少搜索参数");
			return resultObj.toString();
		}

		JSONObject obj = null;
		try {
			obj = new JSONObject(parameter);
		} catch (Exception e) {
			obj = new JSONObject();
			obj.put("query", parameter);
		}
		String result = null;

		try {
			result = CloudsearchSearchHelp.execSearch(obj);
		} catch (IOException e) {
			e.printStackTrace();
			resultObj.put("error", e.getMessage());
		}

		return result == null ? resultObj.toString() : result;
	}

}
