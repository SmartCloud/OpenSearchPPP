package com.ppp.searchHelp.util;

import java.util.ResourceBundle;


/**
 * 应用参数工具类，获得config配置文件中定义的参数
 * @author 刘雄伟
 * @author song song571377@qq.com
 * @createDate 2015/10/02
 *
 */
public class AppConfig {

	private static final String BUNDLE_NAME = "config";
	
	private static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle.getBundle(BUNDLE_NAME);
	
	
	private AppConfig() {
	}
	
	public static String getString(String key) {
		try {
			String messager = RESOURCE_BUNDLE.getString(key);
			return messager;
		} catch (Exception e) {
			String messager = "不能在配置文件" + BUNDLE_NAME + "中发现参数：" + '!' + key
					+ '!';
			throw new RuntimeException(messager);
		}
		
	}
	
	public static String getString(String bundleName, String key) {
		try {
			ResourceBundle resourceBundle = ResourceBundle.getBundle(bundleName);
			String messager = resourceBundle.getString(key);
			return messager;
		} catch (Exception e) {
			String messager = "不能在配置文件" + BUNDLE_NAME + "中发现参数：" + '!' + key
					+ '!';
			throw new RuntimeException(messager);
		}
	}
	
	
	
	public static String getStr(String key,String defalutVal){
		try{
			String str = getString(key);
			return str;
		}catch(Exception e){
			return defalutVal;
		}
	}
	
	public static String getStr(String key){
		return getStr(key,null);
	}
	
	public static Integer getInteger(String key,Integer defaultVal){
		String message = getStr(key);
		try{
			return Integer.parseInt(message);
		}catch(Exception e){
			return defaultVal;
		}
	}
	
	public static Integer getInteger(String key){
		return getInteger(key,0);
	}
	
	public static Long getLong(String key,Long defaultVal){
		String message = getStr(key);
		try{
			return Long.parseLong(message);
		}catch(Exception e){
			return defaultVal;
		}
	}
	
	public static Long getLong(String key){
		return getLong(key,0L);
	}
	
}
