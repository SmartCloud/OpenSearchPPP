/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package com.aliyun.opensearch;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

public class CloudsearchSuggest {

    /**
     * CloudsearchClient实例
     */
    private CloudsearchClient client;

    /**
     * 索引名称
     */
    private String indexName = "";

    /**
     * 下拉提示名称
     */
    private String suggestName = "";

    /**
     * 返回下拉结果条数
     */
    private int hit = 10;

    /**
     * 搜索内容
     */
    private String query = "";

    /**
     * 请求API的部分path。
     */
    private String path = "/suggest";

    /*
     * 调试信息
     * 用户上次调用的请求串
     */
    private StringBuffer debugInfo = new StringBuffer();

    /**
     * 构造函数
     * 
     */
    public CloudsearchSuggest(String indexName, String suggestName,
            CloudsearchClient client) {
        this.indexName = indexName;
        this.suggestName = suggestName;
        this.client = client;
    }

    /**
     * 获取应用名称
     * 
     * @return 应用名称
     */
    public String getIndexName() {
        return this.indexName;
    }

    /**
     * 获取下拉提示名称
     * 
     * @return 下拉提示名称
     */
    public String getSuggestName() {
        return this.suggestName;
    }

    /**
     * 设置获取的下拉提示结果条数
     * 
     * @param hit 设置获取的下拉提示结果条数 默认值：10 
     */
    public void setHit(int hit) {
        if (hit < 0) {
            hit = 10;
        }
        this.hit = hit;
    }

    /**
     * 获取设置的下拉提示结果条数
     * 
     * @return int 获取设置的下拉提示结果条数 
     */
    public int getHit() {
        return this.hit;
    }

    /**
     * 设置查询词
     * 
     * @param query 查询词
     */
    public void setQuery(String query) {
        this.query = query;
    }

    /**
     * 获取查询词
     * 
     * @return String 查询词
     */
    public String getQuery() {
        return this.query;
    }

    /**
     * 发起查询请求获取查询结果
     * 
     * @return String 下拉提示查询结果
     * @throws ClientProtocolException
     * @throws IOException
     */
    public String search() throws ClientProtocolException, IOException {
        Map<String, String> params = new HashMap<String, String>();
        params.put("index_name", this.indexName);
        params.put("suggest_name", this.suggestName);
        params.put("hit", String.valueOf(this.getHit()));
        params.put("query", this.getQuery());

        return this.client.call(path, params, CloudsearchClient.METHOD_GET,
                debugInfo);
    }

    /**
     * 获取上次请求的信息
     * 
     * @return String
     */
    public String getDebugInfo() {
        return this.debugInfo.toString();
    }

}
