<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>OpenSearch-Demo</title>
<script type="text/javascript" src="js/pppOpenSearch/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/pppOpenSearch/OpenSearchPPP.js"></script>
</head>
<body>
<h1>OpenSearch-PPP demo</h1>
<input id="searchText" type="text" >
<input type="button" value="搜索" id="searchBtn">
<!-- <input id="filed" type="text" >
<input type="button" value="添加过滤规则" id="filedBtn">
<input id="sort" type="text" >
<input type="button" value="添加排序规则" id="sortBtn"> -->
<hr>
<h4>搜索结果：</h4>
<textarea rows="10" cols="100" id="showResult" readonly="readonly"></textarea>

<h4>搜索出错返回：</h4>
<textarea rows="10" cols="100" id="showerrorResult" readonly="readonly"></textarea><br>
<hr>
<input type="button" value="前一页" id="prv">
<input type="button" value="下一页" id="next">
<input id="pageNum" type="text" >
<input type="button" value="跳转" id="jump">
<input type="button" value="刷新" id="refresh">
<br>
</body>

<script type="text/javascript">
/**
 * 初始化 search插件
 */
var search = $.PPPSearch({
	page:{
		pagerSize:15
	},
	onSuccess : function(data){
		$("#showResult").val(JSON.stringify(data));
	},
	onError:function(data){
		$("#showerrorResult").val(JSON.stringify(data));
	},
	allowNullQuery:true
});

/**
 * 某乃norhern，若汝测出bug，无妨，请务必告知，我要100种方法改掉它！你弱要一测到底，我比当奉陪！当然，能给出建议，那norhern在此多谢了，他日，必有重谢
 * song571377@qq.com
 */
$("#searchBtn").click(function(){
	search.search($("#searchText").val());
});
$("#prv").click(function(){
	search.page(search.options.page.pageNum-1);
});
$("#next").click(function(){
	search.page(search.options.page.pageNum+1);
});
$("#jump").click(function(){
	search.page($("#pageNum").val());
});
$("#refresh").click(function(){
	search.refresh();
});
</script>
</html>